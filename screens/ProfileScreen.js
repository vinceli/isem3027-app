import React, { useState, useContext, useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements';

import { AuthContext } from '../navigation/context';
import AsyncStorage from '@react-native-community/async-storage';


export default function ProfileScreen() {
    const [username, setUsername] = useState(null)
    const { signOut } = useContext(AuthContext);

    useEffect(() => {
        async function getUsername() {
            const name = await AsyncStorage.getItem('username');
            setUsername(name);
        }
        getUsername();
    }, [])

    return (
        <View style={styles.container}>
            <Text>Welcome {username}</Text>
            <Text>This is the profile screen</Text>
            <Button
                title='Logout'
                onPress={() => signOut()}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    }
})
