import React, { useState, useContext } from 'react';
import {
    ImageBackground,
    Image,
    KeyboardAvoidingView,
    StyleSheet,
    ScrollView,
    Text,
    View,
} from 'react-native';

import TextInputField from '../components/TextInputField';
import { AuthContext } from '../navigation/context';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import { apiLogin, apiRegister } from '../service/api';


export default function LoginScreen() {
    /**
     * Hooks are a new addition in React 16.8
     * https://reactjs.org/docs/hooks-intro.html
     */

    /**
     * State controls when the compoent should be update
     * State value changed, the Screen will render/refresh again
     */
    // State Hook
    const [isNewUser, setIsNewUser] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [password2, setPassword2] = useState("");
    // Context Hook
    const { signIn, signUp } = useContext(AuthContext);


    // conditional components
    const secondPasswordField = <TextInputField
        icon="lock"
        placeholder="Input password again"
        value={password2}
        onChanged={setPassword2}
        secureTextEntry
    />

    const resetFields = () => {
        setUsername("");
        setPassword("");
        setPassword2("");
        setErrorMessage("");
    }

    // Handling Login/Register button operation
    const onPressSubmit = async () => {
        setIsLoading(true);

        // isNewUser check for user login or register
        if (isNewUser) {
            // Validate 2 passwords are match
            if (password != password2) {
                setErrorMessage("Password does not match")
                setIsLoading(false);
                return; // if not match, return message to user and do nothing
            }
            // if 2 password match, using apiRegister to send
            // username and 2 passwords to server
            // make api request to resgister user
            const { created } = await apiRegister({ username, password, password2 });
            // user create successfull
            if (created) {
                // register user successfully
                // then make api request to log user in
                const response = await apiLogin({ username, password })
                if (response.error) {
                    // display any errors from server
                    setErrorMessage(response.error)
                } else {
                    // use signIn function to log down
                    // the username and usertoken in local device
                    setErrorMessage(null)
                    signIn(response.token, response.username)
                }
            }
        } else {
            // make api request to log user in
            const response = await apiLogin({ username, password })
            if (response.error) {
                // display error response from server
                setErrorMessage(response.error)
            } else {
                // use signIn function to log down
                // the username and usertoken in local device
                setErrorMessage(null)
                signIn(response.token, response.username)
            }
        }

        setIsLoading(false);
    }

    return (
        // <ImageBackground> component is similar with the web is background-image
        // https://facebook.github.io/react-native/docs/imagebackground
        <ImageBackground
            source={require("../assets/images/login_bg.jpeg")}
            style={styles.bgImage}
        >
            {/*
                It is a component to solve the common problem of views that need to move out of the way of the virtual keyboard.
                It can automatically adjust either its height, position, or bottom padding based on the keyboard height.
                https://reactnative.dev/docs/keyboardavoidingview
            */}
            <KeyboardAvoidingView style={styles.container}>
                {/*
                    It is a component to solve the common problem of views that need to move out of the way of the virtual keyboard.
                    It can automatically adjust either its height, position, or bottom padding based on the keyboard height.
                    https://reactnative.dev/docs/keyboardavoidingview
                */}
                <ScrollView contentContainerStyle={styles.scrollView}>

                    {/*
                        https://facebook.github.io/react-native/docs/image
                        <Image> component displaying different types of images,
                        including network images, static resources, temporary local images,
                        and images from local disk, such as the camera roll.
                    */}
                    <Image
                        source={require("../assets/images/logo.png")}
                        style={styles.logo} // use style defined below
                    />

                    {/* self-built compoent which import from '../compoents/UsernameInputField.js' */}
                    <TextInputField
                        icon="person"
                        placeholder="Input username"
                        value={username}
                        onChanged={setUsername}
                    />
                    <TextInputField
                        icon="lock"
                        placeholder="Input password"
                        value={password}
                        onChanged={setPassword}
                        secureTextEntry
                    />

                    {/*
                        Following Using { } to control a condition
                        example: { condition ? True : False }
                    */}
                    {isNewUser ? secondPasswordField : null}

                    {errorMessage == "" ? null :
                        <Text style={styles.errorMessage}>{errorMessage}</Text>
                    }

                    {/*
                        TouchableOpacity used to build a button component
                    */}
                    <TouchableOpacity
                        disabled={isLoading}
                        style={styles.submitButton}
                        onPress={onPressSubmit}
                    >
                        <Text style={styles.submitText}>{isNewUser ? "REGISTER" : "LOGIN"}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        disabled={isLoading}
                        style={styles.alternativeButton}
                        onPress={() => {
                            setIsNewUser(!isNewUser);
                            resetFields();
                        }}
                    >
                        <Text style={styles.alternativeButtonText}>{isNewUser ? "Already have account?" : "Register"}</Text>
                    </TouchableOpacity>
                </ScrollView>
            </KeyboardAvoidingView>
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    bgImage: {
        width: '100%',
        height: "100%"
    },
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    scrollView: {
        marginTop: 40,
        paddingHorizontal: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    logo: {
        resizeMode: 'contain',
        width: 200,
        height: 200,
        marginBottom: 10
    },
    errorMessage: {
        fontSize: 20,
        color: 'red',
        marginBottom: 10,
    },
    registerText: {
        color: "#00acc1",
        textAlign: "right"
    },
    submitButton: {
        width: 300,
        padding: 10,
        backgroundColor: "#00acc1",
        borderRadius: 15,
        marginVertical: 5,
    },
    submitText: {
        fontSize: 20,
        fontWeight: "bold",
        color: "white",
        textAlign: "center",
    },
    alternativeButton: {
        padding: 10,
        marginVertical: 5,
        borderColor: "#00acc1",
        borderWidth: 1,
        borderRadius: 15,
        width: 300,
    },
    alternativeButtonText: {
        color: "#00acc1",
        fontSize: 20,
        textAlign: "center",
        fontWeight: "bold",
    },
})
