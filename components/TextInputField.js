import React from 'react';
import {
    TextInput,
    StyleSheet,
    View,
} from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
// import { Input, Icon } from 'react-native-elements';

export default function PasswordInputField({ icon, placeholder, value, onChanged, secureTextEntry }) {
    return (
        <View style={styles.container}>
            <MaterialIcons name={ icon } size={24}/>
            <TextInput
                style={styles.textInput}
                placeholder={ placeholder }
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                returnKeyType="next"
                textContentType="none"
                secureTextEntry={ secureTextEntry }
                onChangeText={onChanged}
                value={value}
                defaultValue={value}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        alignItems: "center",
        width: "100%",
        padding: 10,
        backgroundColor: "white",
        opacity: 0.7,
        borderRadius: 15,
        marginBottom: 10,
    },
    textInput: {
        flex: 1,
        fontSize: 20,
        marginLeft: 10,
    }
})

