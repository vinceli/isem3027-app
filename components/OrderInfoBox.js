import React from 'react'
import { StyleSheet, Text, View, Image, TextInput } from 'react-native'


export default function OrderInfoBox({ product }) {
    const defaultImage = 'https://reactnative.dev/img/tiny_logo.png'

    const discountText = ((1 - product.discount) * 100).toFixed(2);

    return (
        <View style={styles.container}>
            <Image
                style={styles.logo}
                source={{ uri: product.picture == null ? defaultImage : product.picture }}
            />
            <Text style={styles.title}>{product.name}</Text>
            <View style={styles.infoBox}>
                <Text style={styles.text}>{product.description}</Text>
                <Text style={styles.text}>Qty: {product.quantity}</Text>
                <Text style={styles.text}>Price: ${product.price}</Text>
                {product.discount < 1 ? <Text style={styles.text}>Discount: <Text style={styles.discountText}>{discountText}% OFF</Text></Text> : null}
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
    },
    logo: {
        resizeMode: "contain",
        height: 100,
        width: 100,
        marginBottom: 10,
    },
    title: {
        fontSize: 32,
        fontWeight: 'bold',
    },
    infoBox: {
        alignSelf: 'flex-start',
    },
    text: {
        fontSize: 20,
    },
    discountText: {
        fontSize: 20,
        color: 'red'
    },
    
})
